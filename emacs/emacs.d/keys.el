;; Abort commands with Esc
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Indent newline with Return
(global-set-key (kbd "RET") 'newline-and-indent)
